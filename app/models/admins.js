'use strict';

var mongoose = require('mongoose');
var validate = require('mongoose-validator');
const validations = require("../lib/middlewares/validations");

var AdminsModel = function () {

  var adminsSchema = mongoose.Schema({
    email: { type: String, required: [true, "Email is required"], validate:validations.emailValidator},
    firstName: { type: String, required: true },
    lastName: { type: String },
    phoneNumber: { type: String, required: [true, 'Phone Number is required'], unique: [true, 'phoneNumber already exists'], validate: validations.phoneNumberValidator },
    password: { type: String },
  }, { collection: 'Admins', timestamps: true });

  adminsSchema.virtual('fullName').
    get(() => { return `${this.firstName} ${this.lastName}`; }).
    set((v) => {
      const firstName = v.substring(0, v.indexOf(' '));
      const lastName = v.substring(v.indexOf(' ') + 1);
      this.set({ firstName, lastName });
    });
  return mongoose.model('Admins', adminsSchema);

};

module.exports = new AdminsModel();
