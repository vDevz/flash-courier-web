'use strict';

var mongoose = require('mongoose');
var validate = require('mongoose-validator');
const validations = require("../lib/middlewares/validations");
var RidersModel = function () {

  var ridersSchema = mongoose.Schema({
    email: { type: String, required: [true, "Email is required"], validate:validations.emailValidator },
    firstName: { type: String, required: true },
    lastName: { type: String },
    primaryPhNo: { type: String, required: [true, 'Phone Number is required'], unique: [true, 'phoneNumber already exists'], validate: validations.phoneNumberValidator },
    secondaryPhNo: { type: String, validate: validations.phoneNumberValidator },
    password: { type: String },
    cnic: { type: String, validate: validations.cnicValidator },
    address: { type: String, validate: validations.addressValidator },
    profilePic: { type: String, validate: validations.urlValidator},
    zoneId: { type: mongoose.Schema.Types.ObjectId, ref: "Zones" }
  }, { collection: 'Admins', timestamps: true });

  return mongoose.model('Riders', ridersSchema);

};

module.exports = new RidersModel();
