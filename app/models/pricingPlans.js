'use strict';

var mongoose = require('mongoose');

var PricingPlansModel = function () {

  var pricingPlansSchema = mongoose.Schema({
    name: { type: String },
    amount: { type: Number }
  }, { collection: 'PricingPlans', timestamps: true });

  return mongoose.model('PricingPlans', pricingPlansSchema);

};

module.exports = new PricingPlansModel();
