'use strict';

var mongoose = require('mongoose');
var validate = require('mongoose-validator');
const validations = require("../lib/middlewares/validations");

var ClientsModel = function () {

  var clientsSchema = mongoose.Schema({
    companyName: { type: String, required: [true, 'Company name is required'],validate: validations.companyNameValidator },
    contactPersonName: { type: String, required: [true, 'Contact person name is required'], validate: validations.contactNameValidator },
    address: { type: String, required: [true, 'Address is required'],validate: validations.addressValidator },
    primaryPhoneNo: { type: String, required: [true, 'Primary phone is required'], validate: validations.phoneNumberValidator },
    secondaryPhoneNo: { type: String, validate: validations.phoneNumberValidator },
    cnic: { type: String, required: [true, 'CNIC number is required'], validate: validations.cnicValidator },
    ntnNumber: { type: String },
    strnNumber: { type: String },
    url: { type: String, validate: validations.cnicValidator },
    city: { type: String, default: "Lahore"},
    natureOfAccount: { type: String, required: [true, 'Nature of account is required'] },
    natureOfParcels: [{
      type: String, enum: [
        "Apparel", "Automotive Parts", "Accessories",
        "Personal Electronics",
        "Electronics Accessories",
        "Gadgets", "Jewwllery", "Cosmetics",
        "Stationery", "Handicrafts",
        "Home made items", "Footwear",
        "Watches", "Leather Items", "Organic and health products",
        "Appliances & Consumer Electronics", "Home decor and Interior Design Items",
        "Toys", "Pet supplies", "Fitness Items", "Vouchers & Coupons", "Market place", "Documents & Letters", "Others"
      ],
      averageShipmentsMonthly: { type: Number }
    }],
    shippingInformation: {
      pickUpAddress: { type: String, required: [true, 'Pickup address is required'] },
      email: { type: String, required: [true, 'Email address is required'], validate: validations.emailValidator },
      operationTimings: { type: String, required: [true, "Operation Timings is required"] },
      shipperCity: { type: String, required: [true, "Shipper City is required"] },
    },
    bankDetails: {
      bankName: { type: String, required: [true, "Bank name is required"] },
      accountTitle: { type: String, required: [true, "Account title is required"], validate:validations.accountTitleValidator },
      branchName: { type: String, required: [true, "Branch name is required"], validate: validations.branchNameValidator },
      ibanNumber: { type: String, required: [true, "IBAN number is required"], validate: validations.ibanNumberValidator },
      accountNumber: { type: Number, required: [true, "Account number is required"] },
      backCity: { type: String, required: [true, "Account number is required"] },
      cycleOfPayment: { type: String, enum: ["Daily", "Weekly", "Monthly", "Fortnightly"], required: [true, "Cycle of payment is required"] }
    },
    documents: {
      signedDocument: { type: String, validate:validations.urlValidator },
      acknowledgementForm: { type: String, validate: validations.urlValidator },
      cnicFront: { type: String, validate: validations.urlValidator },
      cnicBack: { type: String, validate: validations.urlValidator },
      bankCheque: { type: String, validate: validations.urlValidator }

    }
  }, { collection: 'Clients', timestamps: true });

  return mongoose.model('Clients', clientsSchema);

};

module.exports = new ClientsModel();
