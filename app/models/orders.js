'use strict';

var mongoose = require('mongoose');

var OrdersModel = function () {

  var ordersSchema = mongoose.Schema({
    trackingId: { type: String },
    status: { type: String },
    client: {
      name: { type: String },
      origin: { type: String },
      destination: { type: String },
      reason: { type: String },
      serviceType: { type: String }
    },
    consigneeInfo: {
      name: { type: String },
      phone: { type: String },
      address: { type: String }
    },
    payment: {
      collectionAmount: { type: String },
      bookingDate: { type: Date },
      instruction: { type: String },
      cancelationRemarks: { type: String },
      paymentPlan: { type: String }
    }
  }, { collection: 'Orders', timestamps: true });

  return mongoose.model('Orders', ordersSchema);

};

module.exports = new OrdersModel();
