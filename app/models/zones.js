'use strict';

var mongoose = require('mongoose');

var ZonesModel = function () {

  var zonesSchema = mongoose.Schema({
    name: { type: String },
    coordinates: [
      {
        latitude: { type: Number },
        longitude: { type: Number }
      },
    ],
    pricing: { type: mongoose.Schema.Types.ObjectId, ref: "PricingPlans" }
  }, { collection: 'Zones', timestamps: true });

  return mongoose.model('Zones', zonesSchema);

};

module.exports = new ZonesModel();
