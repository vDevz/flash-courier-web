const { responses } = global;
const bcrypt = require('bcryptjs');
var validate = require('mongoose-validator');
module.exports = {
  companyNameValidator: [
    validate({
      validator: 'isLength',
      arguments: [0, 300],
      message: 'Company name should be between {ARGS[0]} and {ARGS[1]} characters!',
    }),
    validate({
      validator: 'isAlphanumeric',
      passIfEmpty: true,
      message: 'Company name should contain alpha-numeric characters only!',

    }),
  ],
  contactNameValidator :[
    validate({
      validator: 'isLength',
      arguments: [0, 300],
      message: 'Contact person name should be between {ARGS[0]} and {ARGS[1]} characters!',
    }),
    validate({
      validator: 'isAlphanumeric',
      passIfEmpty: true,
      message: 'Contact person name should contain alpha-numeric characters only!',
    })
  ],
  addressValidator:[
    validate({
      validator: 'isLength',
      arguments: [0, 300],
      message: 'Address should be between {ARGS[0]} and {ARGS[1]} characters!',
    })
  ],
  phoneNumberValidator:[
    validate({
      validator: 'isLength',
      arguments: [9, 14],
      passIfEmpty: true,
      message: 'Address should be between {ARGS[0]} and {ARGS[1]} characters!',
    })
  ],
  cnicValidator: [
    validate({
      validator: 'isLength',
      arguments: [16],
      passIfEmpty:true,
      message: 'Address should be 15 characters long!'
    })
  ],
  urlValidator: [
    validate({
      validator: 'isURL',
      message: 'Please provide a valid url!',
      protocols:['http','https','ftp']
    })
  ],
  emailValidator: [
    validate({
      validator: 'isEmail',
      passIfEmpty: true,
      message: 'Please provide a valid email!'
    })
  ],
  accountTitleValidator: [
    validate({
      validator: 'isLength',
      arguments: [0, 300],
      message: 'Account title should be between {ARGS[0]} and {ARGS[1]} characters!',
    }),
    validate({
      validator: 'isAlphanumeric',
      passIfEmpty: true,
      message: 'Account title should contain alpha-numeric characters only!',

    })
  ],
  branchNameValidator: [
    validate({
      validator: 'isLength',
      arguments: [0, 300],
      message: 'Branch name should be between {ARGS[0]} and {ARGS[1]} characters!',
    }),
    validate({
      validator: 'isAlphanumeric',
      passIfEmpty: true,
      message: 'Branch name should contain alpha-numeric characters only!',

    })
  ],
  ibanNumberValidator: [
    validate({
      validator: 'isLength',
      arguments: [0, 300],
      message: 'IBAN number should be between {ARGS[0]} and {ARGS[1]} characters!',
    }),
    validate({
      validator: 'isAlphanumeric',
      passIfEmpty: true,
      message: 'IBAN number should contain alpha-numeric characters only!',

    })
  ],
  signIn: async (req, res, next) => {

    if ((!req.body.email && !req.body.cnic) || !req.body.password) {
      return res.http400(responses.credentialsMissing);
    }

    if (req.body.cnic) {
      var user = await db.Users.findOne({ 'cnic': req.body.cnic }).lean();
      if (user && user.role !== "owner") {
        return res.http400(responses.emailIsNotProvided);
      }
    } else if (req.body.email) {
      var user = await db.Users.findOne({ 'email': req.body.email }).lean();
      if (user && user.role === "owner") {
        return res.http400(responses.cnicIsNotProvided);
      }
    }

    if (!user) user = await db.InsuranceCompanies.findOne({ 'email': req.body.email }).lean();

    if (!user) return res.http400(responses.userDoesNotExists);

    if (!bcrypt.compareSync(req.body.password, user.password)) return res.http401(responses.passwordInvalid);

    res.locals.user = user
    next();
  },
  checkEmptyBody: async (req, res, next) => {
    if (Object.keys(req.body).length == 0) {
      return res.http400("Request body is empty!")
    }
    next();
  }
}

