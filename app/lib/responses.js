module.exports = {
  emptyRequestBody: "No data provided in body!",
  emailAlreadyExists: "Email address already exists!",
  phoneNoAlreadyExists: "Phone Number already exists!",
  userDoesNotExists: "Email or password is invalid!",
  passwordInvalid: "Email or password is invalid!",
  passwordIsNotProvided: "Password is required!",
  emailIsNotProvided: "Ëmail is required!",
  cnicIsNotProvided: "CNIC is required!",
  roleIsNotProvided: "Role is required!",
  failedToSaveUser: "Failed to save user!",
  phoneNoLengthMessage: "Phone number length must be min 11 and max 13 characters long!",
  cnicLengthMessage: "CNIC length must be min 11 and max 13 characters long!",
  credentialsMissing: "Email/CNIC and password are required!",
  passwordLengthMessage: "Password must be min 10 and max 15 characters long!",
  roleInvalid: "User role is invalid!",
  noFileAttachedToUpload: "No file attached!",
  pictureUploadedSuccessFully: "Picture uploaded SuccessFully",
  cnicAlreadyExists: "CNIC already exists!",
  //Claim Lodge Responses
  reasonIsNotProvided: "Reason is required!",
  titleIsNotProvided: "Title is required!",
  docUrlIsNotProvided: "Document Url is required!",
  proofDocsIsNotProvided: "Proof Documents are not provided!",
  failedToLodgeClaim: "Failed to lodge claim!",
  animalNotFound: "Animal Not Found!",
  registeredPolicyNotExist: "Registered Policy does not exist!",
  claimIsAlreadyLodged: "Claim is already lodged!",
  animalIdIsNotProvided: "Animal id is not provided against which claim is to be lodged",
  //Get Claim List Response
  noClaimFound:"No Claim Found!",
  //register policy
  policyIsAlreadyRegistered: "Policy is already registerd against this animal",
  // sub agents
  ownerIdIsMissing: "Owner id is not provided!",
  noPendingClaimsFound: "No pending claims ",
  noOwnersfound: "No owners found!"
}
